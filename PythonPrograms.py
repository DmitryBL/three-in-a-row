﻿import os
import random
import colorama
from colorama import Fore, Back
colorama.init()
import time
import math



class Field:
	X = 0
	Y = 0
	Cells = []
	points = 0
	ROR = 0
	iter=0

	def __init__(self, x, y, rangeOfRandome):
		self.ROR = rangeOfRandome
		self.X = x
		self.Y = y
		self.Cells = []

		for i in range(self.Y):					#наполнение ячеек
			arr = []
			rndNumb = 0
			for j in range(self.X):
				rndtmp = random.randrange(1, self.ROR + 1)
				while(rndNumb == rndtmp): rndtmp = random.randrange(1, self.ROR + 1)
				rndNumb = rndtmp
				arr.append(rndNumb)
			self.Cells.append(arr)
			

	def CellToStr(self, i, j):
		return str(self.Cells[i][j])

	def UpdateScreen(self, isWait, log):				#UPDATE SCREEN
		os.system('cls')
		self.iter += 1
		#print("ITER: " + str(self.iter) + "   LOG: " + log)
		print(Back.BLACK)
		print("TRHEE IN ROW")
		print("Your points: " + str(self.points))

		rowg = "+ "
		for j in range(self.X):
			rowg += str(j + 1) + " "
		print(Back.GREEN)
		print(rowg)

		print(Back.BLUE, end='')
		for i in range(self.Y):
			print(Back.GREEN + str(i + 1) + " ", end='')
			row = ""
			for j in range(self.X):
				if(self.Cells[i][j] == 0):
					row+=str(Back.WHITE)
				else:
					row+=str(Back.BLUE)
				row += self.CellToStr(i, j) + " "
			print(row)

		if isWait:
			print(Back.RED)
			print("WAIT")
		else:
			print(Back.GREEN)
			print("Input separated by space: <x1> <y1> <x2> <y2>   to Revert cells")
			print(Back.BLACK)
			self.Input()

	def Input(self):
		s = input().split()
		self.RevertCells(int(s[0])-1, int(s[1])-1, int(s[2])-1, int(s[3])-1)

	def CheckField(self):

		for i in range(self.Y):
			nInRow = 0
			symbol = 0
			for j in range(self.X):
				if(self.Cells[i][j] != symbol or j+1==self.X):
					if nInRow >= 3:
						self.RespawnCells(True,i, j-nInRow,j)
						return
					symbol = self.Cells[i][j]
					nInRow = 1
				else:
					nInRow+=1

		for i in range(self.X):
			nInRow = 0
			symbol = 0
			for j in range(self.Y):
				if(self.Cells[j][i] != symbol or j+1==self.Y):
					if nInRow >= 3:
						self.RespawnCells(False,i, j-nInRow,j)
						return
					symbol = self.Cells[j][i]
					nInRow = 1
				else:
					nInRow+=1

		self.UpdateScreen(False, "")

	def RespawnCells(self, isRow, nLine, start, end):#endNotInclude
		len = end - start

		for i in range(start, end):
			if isRow:
				self.Cells[nLine][i] = 0
			else:
				self.Cells[i][nLine] = 0
		self.UpdateScreen(True, "")
		time.sleep(1)
		self.points += math.pow(10,len-2)

		rnd = 0
		for i in range(start, end):
			rndtmp = random.randrange(1, self.ROR + 1)
			while(rnd == rndtmp): rndtmp = random.randrange(1, self.ROR + 1)
			rnd = rndtmp
			if isRow:
				self.Cells[nLine][i] = rnd
			else:
				self.Cells[i][nLine] = rnd
		
		self.CheckField()

	def RevertCells(self, x1,y1,x2,y2):


		tmp = self.Cells[y1][x1]
		self.Cells[y1][x1] = self.Cells[y2][x2]
		self.Cells[y2][x2] = tmp
		
		#f.UpdateScreen(True, " REVERT" + str(tmp))
		#time.sleep(1)

		self.CheckField()



f = Field(7, 6, 4)
f.UpdateScreen(False, "Input")